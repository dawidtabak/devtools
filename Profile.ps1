# Copied from http://www.expatpaul.eu/2014/04/vim-in-powershell/
# Create profile file
# new-item -path $profile -itemtype file -force
# notepad $profile

# profile content
set-alias vim "C:/Program Files (x86)/Vim/Vim74/./vim.exe"

# To edit the Powershell Profile
# (Not that I'll remember this)
Function Edit-Profile
{
    vim $profile
}

# To edit Vim settings
Function Edit-Vimrc
{
    vim $HOME\_vimrc
}