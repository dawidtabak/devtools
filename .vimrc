imap <C-l> <Esc>
vmap <C-l> <Esc>

:let mapleader = "\<Space>"

:nmap <leader>c :vsc Window.CloseDocumentWindow<CR>

:nmap <leader>w :vsc Edit.GoToDefinition<CR>
:nmap <leader>e :vsc View.PopBrowseContext<CR>

:nmap <leader>n :vsc Edit.GoToNextLocation<CR>
:nmap <leader>p :vsc View.GoToPrevLocation<CR>

:nmap <leader>j :vsc Edit.GoToFindResults1NextLocation<CR>
:nmap <leader>k :vsc View.GoToFindResults1PrevLocation<CR>