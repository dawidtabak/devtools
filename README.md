# Dev Tools & Configs
Place to store development environment confgiuration files. 
This includes files and scripts such as .vim and powershell profile.

# Handy tools:
+ PoshGit: https://github.com/dahlbyk/posh-git
+ How2: https://github.com/santinic/how2

# Pacakge manager
OneGet in combination with Chocolatey: http://www.hanselman.com/blog/AptGetForWindowsOneGetAndChocolateyOnWindows10.aspx
Get-PackageProvider -name chocolatey
Use Get-Packge, Find-Package and Install-Package to install packages.
